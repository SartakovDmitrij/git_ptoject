package com.example.calcfragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class CalcActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //add editFragment to main layout
        Fragment editFragment = new EditFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.mainFrame, editFragment);
        fragmentTransaction.commit();

        //add keyboardFragment to main layout
        Fragment keyboardFragment = new KeyboardFragment();
        FragmentTransaction fragmentTransaction1 = getFragmentManager().beginTransaction();
        fragmentTransaction1.add(R.id.simpleKeyboard, keyboardFragment);
        fragmentTransaction1.commit();

        //add ingeneerFragment to main landscape layout
        Fragment ingeneerFragment = new IngeneerFragment();
        FragmentTransaction fragmentTransaction2 = getFragmentManager().beginTransaction();
        fragmentTransaction2.add(R.id.ingeneerKeyboard, ingeneerFragment);
        fragmentTransaction2.commit();


        View editNum = (View) findViewById(R.id.mainFrame);
        registerForContextMenu(editNum);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater menuInflater = null;
        switch (v.getId()) {
            case R.id.mainFrame:
                menuInflater = getMenuInflater();
                menuInflater.inflate(R.menu.context_menu, menu);
                Log.i("Call context menu", "Y");
                break;
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.clear:
                EditText editNum = (EditText) findViewById(R.id.addNumbers);
                editNum.setText("");
                break;
            case R.id.big:
                EditText editNum2 = (EditText) findViewById(R.id.addNumbers);
                editNum2.setTextSize(30);
                break;
        }
        return super.onContextItemSelected(item);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        String showText = (String) item.getTitle();
        Toast.makeText(getApplicationContext(), "clicked menu point:" + showText, Toast.LENGTH_LONG).show();
        return super.onOptionsItemSelected(item);
    }

    //save & restore activity data
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Fragment fragment = getFragmentManager().findFragmentById(R.id.mainFrame);
        EditText Scr = (EditText) fragment.getView().findViewById(R.id.addNumbers);
        String savedText = Scr.getText().toString();
        outState.putString("edited", savedText);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Fragment fragment = getFragmentManager().findFragmentById(R.id.mainFrame);
        EditText Scr = (EditText) fragment.getView().findViewById(R.id.addNumbers);
        Scr.setText(savedInstanceState.getString("edited"));
    }
}
