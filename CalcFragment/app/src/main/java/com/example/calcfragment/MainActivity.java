package com.example.calcfragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        Button btnAuth = (Button) findViewById(R.id.btnAuth);
        btnAuth.setOnClickListener(this);

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        EditText login = (EditText) findViewById(R.id.login);
        EditText password = (EditText) findViewById(R.id.password);
        String log = login.getText().toString();
        String pass = password.getText().toString();
        Log.i("login & password", log + " " + pass);
        checkAuth(log, pass);
    }

    private void checkAuth(String login, String password){
        if (login.equals("uuu") && password.equals("uuu")){
            Intent intent = new Intent(MainActivity.this, CalcActivity.class);
            startActivity(intent);
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), R.string.bad_auth, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
}
