package com.example.calcfragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * Created by Дима on 29.08.2015.
 */
public class KeyboardFragment extends Fragment implements View.OnClickListener {

    EditText Scr; //add numbers
    private float NumberBf; //save screen before button pressed
    private String Operation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_keyboard, null);
    }

    @Override
    public void onStart() {
        super.onStart();

        Scr = (EditText)getActivity().findViewById(R.id.addNumbers);
        //add buttons in array
        Button idList[] = {(Button)getActivity().findViewById(R.id.btn0),
                (Button)getActivity().findViewById(R.id.btn1),
                (Button)getActivity().findViewById(R.id.btn2),
                (Button)getActivity().findViewById(R.id.btn3),
                (Button)getActivity().findViewById(R.id.btn4),
                (Button)getActivity().findViewById(R.id.btn5),
                (Button)getActivity().findViewById(R.id.btn6),
                (Button)getActivity().findViewById(R.id.btn7),
                (Button)getActivity().findViewById(R.id.btn8),
                (Button)getActivity().findViewById(R.id.btn9),
                (Button)getActivity().findViewById(R.id.btnPlus),
                (Button)getActivity().findViewById(R.id.btnMinus),
                (Button)getActivity().findViewById(R.id.btnMultiply),
                (Button)getActivity().findViewById(R.id.btnDivide),
                (Button)getActivity().findViewById(R.id.btnClear),
                (Button)getActivity().findViewById(R.id.btnResult)};
        //loop with add listener for button
        for(Button id : idList){
            id.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnClear: //clear
                Scr.setText("0");
                NumberBf = 0;
                Operation = "";
                break;
            case R.id.btnPlus: //plus
                mMath("+");
                break;
            case R.id.btnMinus: //minus
                mMath("-");
                break;
            case R.id.btnMultiply: //multiple
                mMath("*");
                break;
            case R.id.btnDivide: //divide
                mMath("/");
                break;
            case R.id.btnResult: //equal
                mResult();
                break;
            default:
                String numb = ((Button) v).getText().toString();
                getKeyBoard(numb);
                break;
        }
    }

    //read number and write to edit text
    public void getKeyBoard(String str){
        String ScrCurrent = Scr.getText().toString();
        ScrCurrent += str;
        Scr.setText(ScrCurrent);
    }

    //mMath method convert button value to operation
    public void mMath(String str){
        NumberBf = Float.parseFloat(Scr.getText().toString()); //save screen
        Operation = str; //save operation
        Scr.setText(""); //clear screen
    }

    //result method
    public void mResult(){
        float NumAf = Float.parseFloat(Scr.getText().toString());
        float result;
        float resultTmp = 0;
        if(Operation.equals("-")){
            result = NumberBf - NumAf;
            resultTmp = result;
        }
        if(Operation.equals("+")){
            result = NumAf + NumberBf;
            resultTmp = result;
        }
        if(Operation.equals("/")){
            result = NumberBf / NumAf;
            resultTmp = result;
        }
        if(Operation.equals("*")){
            result = NumAf * NumberBf;
            resultTmp = result;
        }
        Scr.setText(String.valueOf(resultTmp));
    }
}
